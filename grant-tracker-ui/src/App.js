import React, {Component} from 'react';
import {Layout, Menu, Icon} from 'antd';

import './App.css';
import UserContent from "./UserComponents/UserContent";
import AdminContent from './AdminComponents/AdminContent';

const { Header, Sider, Footer, Content } = Layout;
const ADMIN = "admin";
const USER = "user";
const CREATE = "create";

const MY_GRANTS = "my-grants";

class App extends Component {
    state = {
        loggedIn: ADMIN,
        viewActive: CREATE
    };

    debugToggleLogin = () => {
        if (this.state.loggedIn === ADMIN) {
            this.setState({
                loggedIn: USER
            });
        } else {
            this.setState({
                loggedIn: ADMIN
            });
        }
    };

    swapView = (event) => {
        this.setState({
            viewActive: event.key
        });
    };

    render() {
        const USER_MENU = (
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['my-grants']}>
                <Menu.Item key={MY_GRANTS}>
                    <Icon type="user" />
                    <span className="nav-text">My Grants</span>
                </Menu.Item>
            </Menu>
        );

        const ADMIN_MENU = (
            <Menu onClick={(e) => this.swapView(e)} theme="dark" mode="inline" defaultSelectedKeys={[this.state.viewActive]}>
                <Menu.Item key={CREATE}>
                    <Icon type="user" />
                    <span className="nav-text">Create</span>
                </Menu.Item>
            </Menu>
        );

        return (
            <div>
                <Layout>
                    <Sider
                        breakpoint="lg"
                        collapsedWidth="0"
                    >
                        <div className="logo" />
                        { this.state.loggedIn === USER ? USER_MENU : ADMIN_MENU }
                    </Sider>
                    <Layout>
                        <Header style={{ background: '#fff', padding: 0 }}>
                            <img onClick={this.debugToggleLogin} className="metis-logo" alt="Metis Logo" src={"metis-logo.png"} />
                            <span className="user-text">{this.state.loggedIn === USER ? "Sample User" : "Administrator"}</span>
                            <img className="user-image" src={this.state.loggedIn === USER ? "user-icon.png" : "admin-icon.png"} alt="User Image" />
                        </Header>
                        <Content style={{ margin: '24px 16px 0' }}>
                            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                                { this.state.loggedIn === USER ? <UserContent/> : <AdminContent view={this.state.viewActive} /> }
                            </div>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            Metis Foundation ©2018 Created by AiQ Engineering
                        </Footer>
                    </Layout>
                </Layout>
            </div>
        );
    }
}

export default App;
